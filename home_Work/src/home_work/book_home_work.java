package home_work;

import java.util.Random;
import java.util.Scanner;
public class book_home_work {
	public static void main(String[] args) {
		System.out.println();
	}
// interesting number manip
	static void changeNameLater() {
		int number = 4;

		for (int i = 1; i <= number; i++) {
			System.out.print(" " + i);
			for (int j = i + 1; j <= number; j++) {
				System.out.print(" " + j);
			}
			System.out.println(" ");
		}
		for (int x = number; x > 0; x--) {
			System.out.print(" " + x);
			for (int y = x - 1; y > 0; y--) {
				System.out.print(" " + y);
			}
			System.out.println(" ");
		}
	}


	static void bookTargilim() {
		// Book 4556 - Page 37: 4+5
		// prints out sequence from 1 -4
		int number = 5;
		for (int i = 1; i <= number; i++) {
			for (int j = i; j <= number; j++) {
				System.out.print(String.format("%d ", j));
			}
			System.out.println(" ");
		}
		// prints sequence from 4 - 1
		for (int x = 1; x <= number; x++) {
			for (int y = 1; y <= number - x + 1; y++) {
				System.out.print(String.format("%d ", y));
			}
			System.out.println(" ");
		}

	}

}