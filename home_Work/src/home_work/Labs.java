package home_work;

import java.util.Scanner;

// Labs 1 - 10
public class Labs {

	public static void main(String[] args) {
		Lab10e4();
	}
//Creates a class matrix and prints students, class average grades
	static void Lab10e4() {
		int[][] students = new int[20][10];
		for (int i = 0; i < students.length; i++) {
			for (int j = 0; j < students[i].length; j++) {
				students[i][j] = (int) (Math.random() * 21) + 80;
			}
		}printDoubleArray(students);
		
	}
	static void printDoubleArray(int[][]arr) {
		double classSum = 0;
		for (int i = 1; i < arr.length; i++) {
			int gradesSum = 0;
			for (int j = 0; j < arr[i].length; j++) {
				System.out.print(arr[i][j] + " ");
				gradesSum += arr[i][j];
			}
			double average = gradesSum*1.0/arr[i].length;
			classSum += average;
			System.out.println(" Student " + i + " , Grade average: " + average);
			System.out.println();
		}
		System.out.println("Class average: " + classSum * 1.0 / arr.length);
	}

//Reverses an array to opposite order and prints out the result
	static void Lab10e3() {
		int[] arr = {6,8,4,2,7,5};
		int[] res = new int[arr.length];
		
		print(arr);
		reverse(res, arr);
		print(res);
		
	}
	static void reverse(int[]res, int[]arr) {
		int index = res.length-1;
		for (int i = 0; i < arr.length; i++) {
			res[index]=arr[i];
			index--;
		}
	}
	
//Gets an array with duplicate values and returns an array of unique values generated from it.
	static void Lab10e2() {
		int[] arr = { 1, 2, 5, 1, 6, 1, 5, 4, 8 };
		print(arr);

		checkAndMarkDuplicates(arr);
		print(arr);

		System.out.println("count : " + countDuplicates(arr));
		int[] res = new int[arr.length - countDuplicates(arr)];
		print(res);

		smartCopy(res, arr);
		print(res);

	}
	// prints function for empty arrays
	public static void print(int[] arr) {

		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
	
	// prints arrays and fills them with random 1-100 numbers
		public static void printFill(int[] arr) {
			for (int i = 0; i < arr.length; i++) {
				arr[i] = randomizerFunction();
				System.out.print(arr[i] + " ");
				
			}
			System.out.println();
		}

	public static void checkAndMarkDuplicates(int[] arr) {
		for (int i = 0; i < arr.length; i++) {
			for (int j = i + 1; j < arr.length; j++) {
				if (arr[i] == -1) {
					continue;
				}
				if (arr[i] == arr[j]) {
					arr[j] = -1;

				}
			}
		}

	}

	public static int countDuplicates(int[] arr) {
		int count = 0;
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] == -1) {
				count++;
			}
		}
		return count;
	}

	public static void smartCopy(int[] res, int[] arr) {
		int idx = 0;
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] != -1) {
				res[idx] = arr[i];
				idx++;
			}
		}
	}

//Creates array and parses over to print out selected numbers
	static void Lab10e1() {
		int[] arr = {1,2,3,1,2,3,3,2,1,9,5,1,1,9};
		int num3 = 0;
		int num1 = 0;
		
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] == 3) {
				num3++;
			}
			else if (arr[i] == 1) {
				num1++;
			}
		}
		System.out.println("The amount of thirds is: " + num3 + "\nThe amount of ones is: " + num1);
	}
	
//Creates a random array of 50 and prints out the high/low values
	static void Lab9e6() {
		int max = 0;
		int min = 100;
		int indexMax = 0;
		int indexMin = 0;

		int[] arr = new int[50];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = randomizerFunction();
			if (arr[i] > max) {
				max = arr[i];
				indexMax = i;
			} else if (arr[i] < min) {
				min = arr[i];
				indexMin = i;
			}
		}
		System.out.println("The max number is: " + max + " index number: " + indexMax);
		System.out.println("The min number is: " + min + " index number: " + indexMin);
	}
	
//Creates a random array and prints out sum of all odd, and even numbers, prints out which sum is greater
	static void Lab9e5() {
		int evenSum = 0;
		int oddSum = 0;
		int[] arr = new int[10];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = randomizerFunction();
			if (arr[i] % 2 == 0) {
				evenSum += arr[i];
			} else {
				oddSum += arr[i];
			}
		}
		System.out.println("Even sum: " + evenSum + "\nOdd sum: " + oddSum + 
				"\nThe greater one is: " + Math.max(evenSum, oddSum));
	}

// Creates a random array and prints out the sum of all odd numbers
	static void Lab9e4() {
		int[] arr = new int[10];

		int sum = 0;
		for (int i = 0; i < arr.length; i++) {
			arr[i] = randomizerFunction();
			if (arr[i] % 2 != 0) {
				System.out.println("Odd number " + arr[i]);
				sum += arr[i];

			}
		}
		System.out.println("The sum of all odd numbers is: " + sum);

	}

//Creates a random array and prints out the sum of all even numbers
	static void Lab9e3() {
		int[] arr = new int[10];
		
		int sum = 0;
		for (int i = 0; i < arr.length; i++) {
			arr[i] = randomizerFunction();
			if (arr[i] % 2 == 0) {
				System.out.println("Even number " + arr[i]);
				sum += arr[i];

			}
		}
		System.out.println("The sum of all even numbers is: " + sum);

	}
	
//Creates a random array and prints all odd numbers
	static void Lab9e2() {
		int[] arr = new int[10];

		for (int i = 0; i < arr.length; i++) {
			arr[i] = randomizerFunction();
			if (arr[i] % 2 != 0) {
				System.out.println("Odd number " + arr[i]);

			}
		}

	}
	
//Creates random array and prints out all even numbers
	static void Lab9e1 () {
		int[] arr = new int[10];

		for (int i = 0; i < arr.length; i++) {
			arr[i] = randomizerFunction();
			if (arr[i] % 2 == 0) {
				System.out.println("Even number " + arr[i]);

			}
		}

	}
	
//Creates random int array and prints out max & min number and their index, sum of all and the average
	static void Lab8e8() {
		int[] arr = new int[10];

		int sum = 0;
		int max = 0;
		int min = 100; // i gave it the highest number so the only way it can go is down
		int minIndex = 0;
		int maxIndex = 0;

		for (int i = 0; i < arr.length; i++) {
			arr[i] = randomizerFunction();
			sum += arr[i];
			if (arr[i] < min) {
				min = arr[i];
				minIndex = i;
			} else if (arr[i] > max) {
				max = arr[i];
				maxIndex = i;
			}

			System.out.print(arr[i] + " ");
		}
		System.out.println();
		System.out.println("Min number is: " + min + " | index is: " + minIndex);
		System.out.println("Max number is: " + max + " | index is: " + maxIndex);
		System.out.println("The sum of the array is: " + sum * 1.0 + "\nAnd the average is: " + sum * 1.0 / arr.length);
	}

//Creates random int array and prints the lowest number in the stack with its corresponding index number
	static void Lab8e7() {
		int[] arr = new int[10];
		
		int min = 100; // i gave it the highest number so the only way it can go is down
		int index = 0;
		for (int i = 0; i < arr.length; i++) {
			arr[i] = randomizerFunction();
			if (arr[i] < min) {
				min = arr[i];
				index = i;
			}
			System.out.print(arr[i] + " ");
		}
		System.out.println();
		System.out.println("Min number is: " + min + "\nIts corresponding index is: " + index);
		
	}
	
//Creates random int array and prints the highest number in the stack with its corresponding index number
	static void Lab8e6() {
		int[] arr = new int[10];

		int max = 0;
		int index = 0;
		for (int i = 0; i < arr.length; i++) {
			arr[i] = randomizerFunction();
			if (arr[i] > max) {
				max = arr[i];
				index = i;

			}
			System.out.print(arr[i] + " ");
		}
		System.out.println();
		System.out.println("Max number is: " + max + "\nIts index number is: " + index);
	}

// Creates and array with random integers and prints the highest number in the stack
	static void Lab8e5() {
		int[] arr = new int[10];

		int max = 0;
		for (int i = 0; i < arr.length; i++) {
			arr[i] = randomizerFunction();
			if (arr[i] > max) {
				max = arr[i];

			}
			System.out.print(arr[i] + " ");
		}
		System.out.println();
		System.out.println("max : " + max);
	}

// Creates and array with random integers and prints out the sum and average
	static void Lab8e4() {
		int[] arr = new int[10];
		int sum = 0;

		for (int i = 0; i < arr.length; i++) {
			arr[i] = randomizerFunction();
			sum += arr[i];
			System.out.print(arr[i] + " ");
		}
		System.out.println();
		System.out.println("The sum of the array is: " + sum + "\nAnd the average is: " + sum * 1.0 / arr.length);
	}

// Creates and array with random integers and prints out the sum and average
	static void Lab8e3() {
		int[] arr = new int[10];
		int sum = 0;

		for (int i = 0; i < arr.length; i++) {
			arr[i] = (int) (Math.random() * 101);
			sum += arr[i];
			System.out.print(arr[i] + " ");
		}
		System.out.println();
		System.out.println("The sum of the array is: " + sum);
	}

// Creates an array with randomized values print in the same line
	static void Lab8e2() {
		int[] arr = new int[10];

		for (int i = 0; i < arr.length; i++) {
			arr[i] = (int) (Math.random() * 101);
			System.out.print(arr[i] + " ");
		}
	}

// Creates an array with randomized values print new line
	static void Lab8e1() {
		int[] arr = new int[10];

		for (int i = 0; i < arr.length; i++) {
			arr[i] = (int) (Math.random() * 101);
			System.out.println(arr[i]);
		}
	}

	static void Lab6e7() {
		Scanner sc = new Scanner(System.in);
		System.out.println("please input the boomed number");
		int dig = sc.nextInt();

		for (int i = 0; i <= 100; i++) {
			if (i % 7 == 0 || String.valueOf(i).contains("7")) {
				System.out.print("boom ");
			} else {
				System.out.print(i + " ");
			}
			sc.close();
		}

		for (int i = 1; i <= 10; i++) {
			for (int j = 1; j <= 10; j++) {
				int num = i * j;
				if (num % dig == 0) {
					System.out.print("BooM ");
				} else if (num % 10 == dig || num / 10 == dig) {
					System.out.print("boom ");
				} else {
					System.out.print(num + " ");
				}
			}
			System.out.println();
		}
	}

	static void Lab6e4() {
		int num = (int) (Math.random() * 10001);
		System.out.println("The generated number is = " + num);
		int opposite = 0;
		while (num > 0) {
			opposite = opposite * 10 + num % 10;
			num /= 10;
		}
		System.out.println("opposite is " + opposite);
	}

	static void Lab6e3() {
		int num = (int) (Math.random() * 10001);
		System.out.println("num = " + num);
		;

		while (num > 9) {
			num /= 10;
		}
		System.out.println(num);
	}

	static void Lab6e2() {
		int num = randomizerFunction() * 10001;
		System.out.println(String.format("The number is = %d", num));
		System.out.println("The right most digit is = " + num % 10);
	}

	static void Lab6e1() {
		int num = randomizerFunction() * 10001;
		int counter = 0;
		for (int i = 1; i <= num; i++) {
			counter++;
			System.out.println("The number of digits in num is = " + counter);
		}
	}

	static void Lab4e3() {
		// Leap year cicletron
		int year = (int) (Math.random() * 2021);
		if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0) {
			System.out.println(String.format("The year %d is a leap year", year));
		} else {
			System.out.println(String.format("The year %d is not a leap year", year));
		}
	}

	static void Lab4e1() {
		double salary = Math.random() * 150001;
		float tax = 0;

		if (salary <= 23000) {
			tax = (float) 0.1;

		} else if (salary <= 50000 && salary >= 23001) {
			tax = (float) 0.2;

		} else if (salary <= 100000 && salary >= 50001) {
			tax = (float) 0.3;

		} else if (salary > 100001) {
			tax = (float) 0.4;

		}
		System.out.println("The salary: " + salary + "\n" + "the tax cut is " + salary * tax);
		System.out.println("the salary after cut " + (salary - (salary * tax)));
	}

	static void Lab3e5() {
		int x = randomizerFunction();
		int y = randomizerFunction();
		int z = randomizerFunction();
		System.out.println(String.format("The numbers are x %d number y %d and number z %d", x, y, z));

		System.out.println("the lowest number is: " + Math.min(x, Math.min(y, z)));

	}

	static void Lab3e4() {
		int a = randomizerFunction();
		int b = randomizerFunction();
		int c = randomizerFunction();
		System.out.println(String.format("The numbers are a %d number b %d and number c %d", a, b, c));

		if (a > b) {
			System.out.println("The highest is " + Math.max(a, c));
		} else if (b > a) {
			System.out.println("The highest is " + Math.max(b, c));
		} else if (c > b) {
			System.out.println("The highest is " + Math.max(c, a));
		}

	}

	static void Lab3e3() {
		// 5000 -6000

		double sal = (int) (Math.random() * 1001) + 5000;
		System.out.println("Current salary = " + sal);

		if (sal * 1.1 > 6000) {
			sal = sal * 1.05;
		} else {
			sal = sal * 1.10;
		}

		System.out.println("New salary = " + sal);
	}

// ***Enhanced Lab3e2 *** more than required
	static void Lab3e2() {
		int num = 0;
		int secretNum = randomizerFunction();
		int count = 0;
		int oddcount = 0;
		int poscount = 0;

		while (num != secretNum) {
			num = randomizerFunction();
			count++;
			if (num == secretNum) {
				System.out.println("BINGO and the number is " + num);
			} else if (num != secretNum) {
				if (num % 2 == 0) {
					poscount++;
				} else {
					oddcount++;
				}
			}
		}
		System.out.println(String.format("The total number of counts was %d with %d Even nums and %d Odd nums", count,
				poscount, oddcount));
		System.out.println("The percentage of Evens is: " + (float) poscount / count * 100.0 + "%");
		System.out.println("The percentage of Odds is: " + (float) oddcount / count * 100.0 + "%");

	}

	static void Lab3e1() {
		int num = (int) (Math.random() * 101);

		if (num > 50) {
			System.out.println("Big !");
		} else if (num < 50) {
			System.out.println("Small !");
		} else {
			System.out.println("Bingo !");
		}

	}

	static void Lab2e5() {
		int a = randomizerFunction();
		int b = randomizerFunction();
		// This is much better:
		System.out.println(Math.max(a, b));

		// This is less recommended
		if (a > b) {
			System.out.println(a);
		} else if (b > a) {
			System.out.println(b);
		} else {
			System.out.println(String.format("Its a tie! a is %d and b is %d", a, b));
		}

	}

	static void Lab2e4() {
		int time = randomizerFunction() + 250;
		int hours = time / 60;
		int minutes = time % 60;
		System.out.println(String.format("The movie is going to last for %d hours and %d minutes ", hours, minutes));
	}

	static void Lab2e3() {
		int a = randomizerFunction();
		int b = randomizerFunction();

		System.out.println(String.format("The first integer is %d \nand the second integer is %d", a, b) + "\n"
				+ "The sum of the numbers is: " + (a + b) + "\n" + "The average value: " + (a + b) / 2 + "\n"
				+ "The remainder of a " + (a / 10) + "\n" + "The remainder of b " + (b / 10) + "\n"
				+ "The area of a rectangle is " + (a * b));
	}

	static void Lab2e2() {
		String firstString = "there will be ";
		int visitorsInt = 5;
		String secondString = " people for dinner";

		System.out.println(firstString + (visitorsInt + 2) + secondString);

	}

	static void Lab2e1() {
		String firstString = "there will be ";
		int visitorsInt = 5;
		String secondString = " people for dinner";

		System.out.println(firstString + visitorsInt + secondString);

	}

	static int randomizerFunction() {
		int num = (int) (Math.random() * 101);
		return num;
	}

}
