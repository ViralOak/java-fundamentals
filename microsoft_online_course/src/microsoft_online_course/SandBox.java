package microsoft_online_course;

import java.util.Scanner;

public class SandBox {

	public static void main(String[] args) {
		
	}

	public static void print(int n) {
		if (n == 1) {
			System.out.print("< bc >");
		} else {
			System.out.print("p(" + n + ") -> ");
			print(n - 1);
		}
	}

	// Time difference
	public static void timeDifference() {
		int hours = 12 % 24;
		System.out.println(hours);
	}

	// Return calculated data from user input
	public static void travelAndBudget(int time, int money) {
		System.out.println(time + " Days, in hours is " + time * 24 + " or in minutes " + 60 * (time * 24));
		System.out.println("If you have " + money + "$" + " you will spend " + money / time + "$"
				+ " each day for your " + time + " days stay");
		System.out.println("in peso is " + money * 19 + " each day spend " + (money * 19) / time);
	}

	// String scanner
	static String stringScanner() {
		Scanner scan = new Scanner(System.in);
		String str = scan.nextLine();
		return str;

	}

	// Integer scanner
	static int intScanner() {
		Scanner scan = new Scanner(System.in);
		int num = scan.nextInt();
		return num;
	}

}
