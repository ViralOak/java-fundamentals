package learningTheRopes;

import java.util.Random;

import java.util.Scanner;

public class basicFunctions {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		volumeCheck();
	}

// to the power function
	static void toThePower(int base, int exp) {
		int result = 1;
		for (int i = 1; i <= exp; i++) {
			result *= base;
		}
		System.out.println("base to the " + exp + " = " + result);
	}

// Use recursion to add all of the numbers between x to y.	
	public static int recursion(int start, int end) {
		System.out.println(end);
		if (end >= start) {
			return end + recursion(start, end - 1);
		} else {
			return 0;
		}
	}

// Array example
	static void arrayGame() {
		int[] arr = new int[10];

		int sum = 0;
		int max = 0;
		for (int i = 0; i < arr.length; i++) {
			arr[i] = (int) (Math.random() * 101);
			sum += arr[i];
			if (arr[i] > max) {
				max = arr[i];
			}
			System.out.println(arr[i] + " ");
		}
		System.out.println("max is : " + max);
		System.out.println("sum is:  " + sum);
		System.out.println("avg : " + sum * 1.0 / arr.length);
	}

// This prints out num x num tavla
	static void tavlatKefel() {
		for (int i = 1; i <= 10; i++) {
			for (int j = 1; j <= 10; j++) {
				System.out.print(String.format("%3d ", i * j));
				if (i == j) {
					continue;
				}
			}
			System.out.println();
		}
	}

// This checks the age and height of any lad or las wanting to have a ride on a rollercoaster
	static void checkAgeHeight(int age, int height) {
		if (age >= 8 && height >= 115) {
			System.out.println("Ride the rollercoaster");
		} else if (age >= 18 && height >= 100) {
			System.out.println("Ride the rollercoaster");
		} else if (age < 8 | height < 115) {
			System.out.println("You cant ride the coaster");
		} else {
			System.out.println("Something went wrong");
		}
	}

// This function just checks the volume of any given proporsions
	static void volumeCheck() {
		Scanner sc = new Scanner(System.in);

		System.out.println("Please input the Diameter of the object in: cm ");
		int diameter = sc.nextInt();

		System.out.println("Please input the Depth of the object in: cm ");
		int depth = sc.nextInt();

		int R = diameter / 2;
		double S = Math.PI * (R * R);
		double V = S * depth;

		sc.close();
		System.out.println("The volume of the pot is " + V + " ml");
	}

// Fibonachi function
	static void fibonachiFunction() {
		System.out.println("Fibonachi sequence printing");
		Scanner sc = new Scanner(System.in);

		System.out.println("Type in the index number for the sequence");
		int index = sc.nextInt();
		int a1 = 1;
		int a2 = 1;

		while (index - 2 > 0) {
			a2 = a1 + a2;
			a1 = a2 - a1;
			index--;
			System.out.println(a1);
		}
		sc.close();
	}

// ================= TARGIL #1+2 LOOPED CONDITIONS =================
	static void loopedCondition() {
		Scanner sc = new Scanner(System.in);

		System.out.println("Input a top number");
		int top = sc.nextInt();

		for (int i = 1; i <= top; i++) {
			System.out.println(i + " I will loop untill i reach " + top);
		}
		sc.close();
	}

// ================= TARGIL #3 LOOPED CONDITIONS =================
	static void evenNumberPrinter() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Prints out all even numbers from 0 to typed num");

		System.out.println("Input a natural number");
		int number = sc.nextInt();

		for (int i = 0; i <= number; i++) {
			if (i % 2 == 0) {
				System.out.println(i);
			}
		}
		sc.close();
	}

// ================= TARGIL #4 LOOPED CONDITIONS =================
	static void maxDevidedByDen() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Prints if max devides by den");

		System.out.println("Input a den number");
		int den = sc.nextInt();

		System.out.println("Input a max number");
		int max = sc.nextInt();

		for (int i = 1; i <= max; i++) {
			if (i % den == 0) {
				System.out.println("The number devides by den: " + i);
			}
		}
		sc.close();
	}

// ================= TARGIL #5 DATA PROCESSING =================
	static void numberAdder() {
		System.out.println("This function collects input unless -99 is given, 0 is ignored");
		Scanner sc = new Scanner(System.in);

		int sum = 0;

		System.out.println("Input the first number for the adder>> ");
		int num = sc.nextInt();

		while (num != -99) {
			if (num >= 0) {
				sum = sum + num;
			}
			System.out.println("You can type another number, current score is: " + sum);
			num = sc.nextInt(); // << No need to declare "int sum =..." to use it again just add x=sc...
		}
		sc.close();
		System.out.println("Final adder score is: " + sum);
	}

// ================= TARGIL #6 DATA PROCESSING =================
	static void averageSummer() {
		System.out.println("This function collects number and prints out their average in the end");
		Scanner sc = new Scanner(System.in);

		double sum = 0;
		int count = 0;

		System.out.println("Input the first number: ");
		int num = sc.nextInt();

		while (num != 0) {
			System.out.println("Input another number: ");
			num = sc.nextInt();
			sum = sum + num;
			count++; // << TO NOTE THAT YOU CAN ADD TO A COUNTER SIMPLY WITH ++
			System.out.println("The count is: " + count + " the sum is: " + sum);
		}
		sc.close();
		System.out.println("The average: " + sum / count);
	}

// ================= TARGIL #7 DATA PROCESSING =================
	static void maxNumberPrinter() {
		System.out.println("This function collects numbers and prints out the highest one in the end");
		Scanner sc = new Scanner(System.in);

		System.out.println("Input the first number: ");
		int num = sc.nextInt();
		int max = 0;
		// (max = num) This is so that the first number will be counted as-well
		max = num;

		while (num > 0) {
			System.out.println("Input another number: ");
			num = sc.nextInt();
			if (num > max) {
				max = num;
			}
		}
		sc.close();
		System.out.println("The highest number is: " + max);
	}

// ================= TARGIL #8 DATA PROCESSING =================	
	static void minNumberPrinter() {
		System.out.println("This function collects numbers and prints out the lowest one out of the bunch");
		Scanner sc = new Scanner(System.in);

		int min = 0;

		System.out.println("Input the first number: ");
		int num = sc.nextInt();

		min = num;
		while (num > 0) {
			System.out.println("Input another number: ");
			num = sc.nextInt();
			if (num < min) {
				min = num;
				System.out.println("Currently the lowest one is: " + min);
			}
		}
		sc.close();
		System.out.println("While loop ended, the lowest number is: " + min);
	}

//================= TARGIL #9 DATA PROCESSING =================
	static void randomMaxNumber() {
		System.out.println("Prints 100 random numbers and the highest one with its serial position");
		Random rand = new Random();

		int num = rand.nextInt();
		int max = 0;
		int count = 0;
		int serial = 0;

		while (count <= 100) {
			num = rand.nextInt(100);
			count++;
			System.out.println("Random number : " + num + " The serial position is: " + count);
			if (num > max) {
				max = num;
				serial = count;
			}
		}
		System.out.println("Max number is: " + max + " It's serial number is: " + serial);
	}

// ================= TARGIL #10 WHOLE NUMBER PROCESSING =================
	static void leftNumberPrint() {
		System.out.println("This function prints out the leftmost number from prompt");
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter a random number");
		int num = sc.nextInt();

		while (num > 9) {
			num = num / 10;
		}
		sc.close();
		System.out.println(num);
	}

// ================= TARGIL #11 WHOLE NUMBER PROCESSING =================
	static void digitQuantity() {
		System.out.println("Counts the number of digits a scanned number has");
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter a random number");
		int num = sc.nextInt();
		int count = 0;

		while (num > 0) {
			num = num / 10;
			count++;
		}
		sc.close();
		System.out.println("Quantity of digits in the number " + count);
	}

// ================= TARGIL #12 WHOLE NUMBER PROCESSING =================
	static void enumerateOverNum() {
		System.out.println("Enumerates over number and adds all together");
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter a random number");
		int num = sc.nextInt();
		int sum = 0;

		while (num > 0) {
			sum = sum + num % 10;
			num = num / 10;
		}
		sc.close();
		System.out.println("The number adds up to " + sum);
	}

// ================= TARGIL #13 WHOLE NUMBER PROCESSING =================
	static void selectedNumCounter() {
		System.out.println("Takes tow number and displays how many time each goes into each other");
		Scanner sc = new Scanner(System.in);

		System.out.println("Type in a number to enumerate over");
		int num = sc.nextInt();

		System.out.println("Type in the selected number");
		int dig = sc.nextInt();
		int count = 0;

		while (num > 0) {
			if (dig == num % 10) {
				count++;
			}
			num = num / 10;
		}
		sc.close();
		System.out.println("the selected digit was: " + count + " times in the given number");
	}

// ================= TARGIL #14 WHOLE NUMBER PROCESSING =================
	static void oppositeNumber() {
		System.out.println("Prints the opposite sequence of the entered number");
		Scanner sc = new Scanner(System.in);

		System.out.println("Type in a number");
		long num = sc.nextLong();
		long sum = 0;

		while (num > 0) {
			sum = sum * 10 + num % 10;
			System.out.println(sum);

			num = num / 10;
			System.out.println(num);
		}
		sc.close();
		System.out.println("The opposite number is: " + sum);
	}

// ================= TARGIL #15 WHOLE NUMBER PROCESSING =================
	static void palindromeBool() {
		System.out.println("This is a palindrome checker prints out T/F");
		Scanner sc = new Scanner(System.in);

		System.out.println("Type in a number ");
		int num = sc.nextInt();
		int sum = 0;
		int opposite = 0;
		sum = num;

		while (num > 0) {
			opposite = opposite * 10 + num % 10;
			num = num / 10;
		}
		if (sum == opposite) {
			System.out.println("True! Palindrome");
		} else if (sum != opposite) {
			System.out.println("False! Not Palindrome");
		}
		sc.close();
	}

////================= TARGIL #16 MATH =================
	static void multiplyNoOperator() {
		System.out.println("This takes two ints and multiplies them without * operator");
		Scanner sc = new Scanner(System.in);

		System.out.println("Type in the first number");
		int num1 = sc.nextInt();

		System.out.println("Type in the second number");
		int num2 = sc.nextInt();

		int sum = 0;
		int tmp = 0;

		if (num1 < num2) {
			tmp = num1;
			num1 = num2;
			num2 = tmp;
		}
		for (int i = 1; i <= num2; i++) {
			sum = sum + num1;
		}
		sc.close();
		System.out.println("The sum of both number is: " + sum);
	}

//================= TARGIL #17 MATH =================
	static void powerNoOperator() {
		System.out.println("This function powers up by 2 without the ** operator");
		Scanner sc = new Scanner(System.in);

		System.out.println("Type in a number to be powered by 2");
		int num = sc.nextInt();
		int sum = 1;

		for (int i = 1; i <= num; i++) {
			sum = num * i;
		}
		sc.close();
		System.out.println("The result is: " + sum);
	}

//================= TARGIL #19 MATH =================
	static void azeretNumber() {
		System.out.println("This does num*num-1*num-2*num-3.etc >> cool");
		Scanner sc = new Scanner(System.in);

		System.out.println("Type in the number");
		int num = sc.nextInt();
		int temp = num;

		while (num > 1) {
			temp = temp * (num - 1);
			num--;
		}
		System.out.println("The azeret with while loop is: " + temp);

		System.out.println("Type in another number for loop");
		int num2 = sc.nextInt();
		int sum = 1;

		for (int i = 1; i <= num2; i++) {
			sum = sum * i;
		}
		sc.close();
		System.out.println("The azeret with for loop is: " + sum);
	}

// Basic Logic, defines numbers by value
	static void logicDefiner(int a, int b, int c) {
		int A = a;
		int B = b;
		int C = c;
		if (A > B && B > C) {
			System.out.println("Bottom is: " + C + "  Middle is: " + B + "  Top is: " + A);
		}
		// A C B
		else if (A > C && C > B) {
			System.out.println("Bottom is: " + B + "  Middle is: " + C + "  Top is: " + A);
		}
		// C A B
		else if (C > A && A > B) {
			System.out.println("Bottom is: " + B + "  Middle is: " + A + "  Top is: " + C);
		}
		// B C A
		else if (B > C && C > A) {
			System.out.println("Bottom is: " + A + "  Middle is: " + C + "  Top is: " + B);
		}
		// B A C
		else if (B > A && A > C) {
			System.out.println("Bottom is: " + C + "  Middle is: " + A + "  Top is: " + B);
		}
		// C A B
		else if (C > A && A > B) {
			System.out.println("Bottom is: " + B + "  Middle is: " + A + "  Top is: " + C);
		}
		// C B A
		else if (C > B && B > A) {
			System.out.println("Bottom is: " + A + "  Middle is: " + B + "  Top is: " + C);
		} else {
			System.out.println("Something went wrong fix it");
		}

	}
	static void switchCaseExample() {
		 // SWITCH CASE month testing
        int month = 8;
        String monthString;
        switch (month) {
            case 1:  monthString = "January";
                     break;
            case 2:  monthString = "February";
                     break;
            case 3:  monthString = "March";
                     break;
            case 4:  monthString = "April";
                     break;
            case 5:  monthString = "May";
                     break;
            case 6:  monthString = "June";
                     break;
            case 7:  monthString = "July";
                     break;
            case 8:  monthString = "August";
                     break;
            case 9:  monthString = "September";
                     break;
            case 10: monthString = "October";
                     break;
            case 11: monthString = "November";
                     break;
            case 12: monthString = "December";
                     break;
            default: monthString = "Invalid month";
                     break;
        }
        System.out.println(monthString);
	}

}
