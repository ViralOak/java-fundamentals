package learningTheRopes;

import java.util.*;

public class Lesson_sandbox {
	public static void main(String[] args) {
		System.out.println("This is the class sandbox, this page resets before each class");
		int[] arr = new int[20];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = (int) (Math.random() * 15);
			System.out.print(arr[i] + " ");
			checkAndMarkDuplicates(arr);
		}
		System.out.println();
		
		for (int j = 0; j < arr.length; j++) {
			if (arr[j] != -1) {
				System.out.print(arr[j] + " ");
			} else {
				continue;
			}

		}
		System.out.println();

	}

	public static void checkAndMarkDuplicates(int[] arr) {
		for (int i = 0; i < arr.length; i++) {
			for (int j = i + 1; j < arr.length; j++) {
				if (arr[i] == -1) {
					continue;
				}
				if (arr[i] == arr[j]) {
					arr[j] = -1;

				}
			}
		}

	}

}