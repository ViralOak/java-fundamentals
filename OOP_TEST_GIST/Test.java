
public class Test {

	public static void main(String[] args) {

		// 1. Create two arrays and initiate them with random values
		int[] array = new int[15];
		int[] array2 = new int[15];
		randomizeArray(array);
		randomizeArray(array2);

		// 2. print both arrays
		print(array);
		print(array2);

		// 4. create a 10 long array named 'distinct' and initiate it with -1 values,
		int[] distinct = new int[10];
		for (int i = 0; i < distinct.length; i++) {
			distinct[i] = -1;
		}

		// add an additional code that will add all the numbers in arr1 that are not
		// present in arr2 and visa versa, each digit needs to be present only once
		// without doubles
		int n = array.length;
		int m = array2.length;
		findUnique(array, array2, n, m);

		// 5. print out distinct (arr3) array
		print(distinct);

	}

	static void findUnique(int a[], int b[], int n, int m) {
		for (int i = 0; i < n; i++) {
			int j;
			for (j = 0; j < m; j++)
				if (a[i] == b[j])
					break;
			if (j == m)
				System.out.print(a[i] + " ");
		}
		System.out.println();
	}

	// 6. create and print out a number out of the distinct array e.g:
	// 1-,1-,1-,1-,1-,1-,2,8,6,4, = 4682

	// Utils Section
	static void reverse(int[] res, int[] arr) {
		int index = res.length - 1;
		for (int i = 0; i < arr.length; i++) {
			res[index] = arr[i];
			index--;
		}
	}

	public static void smartCopy(int[] res, int[] arr) {
		int idx = 0;
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] != -1) {
				res[idx] = arr[i];
				idx++;
			}
		}
	}

	public static void randomizeArray(int[] arr) {
		for (int i = 0; i < arr.length; i++) {
			arr[i] = (int) (Math.random() * 10);
		}
	}

	public static void print(int[] arr) {
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}

	public static void checkAndMarkDuplicates(int[] arr) {
		for (int i = 0; i < arr.length; i++) {
			for (int j = i + 1; j < arr.length; j++) {
				if (arr[i] == -1) {
					continue;
				}
				if (arr[i] == arr[j]) {
					arr[j] = -1;

				}
			}
		}

	}

}
